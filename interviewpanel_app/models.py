from __future__ import unicode_literals

from django.db import models

class JobOpenings(models.Model):
    ApplicationInterviewId = models.CharField(max_length=100)
    External_Job_Description = models.CharField(max_length=10000,default=False)
    jd_old = models.CharField(max_length=10000,default=False)

    def __str__(self):
        return self.External_Job_Description