# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework.response import Response
from . serializers import JobOpeningsSerializers
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from django.shortcuts import render_to_response
from .models import *
from django.views.decorators.csrf import csrf_exempt
import json
import urllib2
import pandas as pd
import re
from rake_nltk import Rake
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import ast
import gensim
import spacy
# import en_core_web_sm
from nltk.util import ngrams
from collections import Counter
from rake_nltk import Rake
import operator
from rake_nltk import rake
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import requests
import pandas as pd
import re
from nltk.corpus import stopwords
from find_job_titles import FinderAcora
from PyDictionary import PyDictionary
from keras.models import load_model
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import os
import sys
import numpy as np
import ast
from textblob import TextBlob

nlp = spacy.load('en_core_web_sm')
NerModel = spacy.load('en', path='/home/nehan/IPRSystemWithNewChanges/Models/NewNERModel/') # Model for Role Extraction
finder=FinderAcora()
dictionary=PyDictionary()

# create the class based views for rest apis
class jobDescriptionList(APIView):
    def get(self,request):
        jobDescription1 = JobOpenings.objects.all()
        serializer = JobOpeningsSerializers(jobDescription1,many=True)
        return Response(serializer.data)
    def post(self,request):
        data = JSONParser().parse(request)
        serializer = JobOpeningsSerializers(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def find_word(text, search):
    result = re.findall('\\b' + search + '\\b', text, flags=re.IGNORECASE)
    if len(result) > 0:
        return True
    else:
        return False

def unique_list(l):
    ulist = []
    [ulist.append(x) for x in l if x not in ulist]
    return ulist


# def verb(string):
#     doc = nlp(string.decode('utf8'))
#     ret_verb = ""
#     for token in doc:
#         if token.pos_ == 'VERB':
#             ret_verb = token.text
#     return ret_verb

sim_list = ['newcomer', 'volunteer', 'appoint', 'hire', 'employ', 'engage', 'enlist', 'obtain',
                            'select','assign', 'book', 'commission', 'require', 'consider']

# def search_syn(string):
#     a = 0
#     try:
#         syn_list = dictionary.synonym(string)
#         for j in syn_list:
#             if j in sim_list:
#                 a = 1
#     except TypeError:
#         pass
#     return a

def interviewList(request):
    applicationIds = JobOpenings.objects.filter().order_by('ApplicationInterviewId')
    applicationIdLi = []
    for i in applicationIds:
        applicationIdLi.append(i.ApplicationInterviewId)
    SkillsLi = ['.net', 'ios', 'mumps & vista', 'java', 'database', 'python',
       'big data', 'react.js', 'scripting', 'build and release deployment',
       'ps/support', 'php', 'ror', 'dba', 'android', 'mobile', 'core java',
       'ui/ux', 'jquery', 'group architect (across tech)', 'angular 2',
       'blockchain', 'c/c++', 'advanced scripting', 'node.js', 'qa',
       'data analyst', 'sales force', 'ba', 'angularjs',
       'machine learning', 'project mgmt', 'marketing/sales',
       'java & vista', 'advanced testing', 'delivery management',
       'ror, node.js', 'java -c++ n&s domain', 'management']
    PositionLi = ['Senior Marketing Executive',
       'Job   Executive Assistant to  Directors', 'Software Engineer',
       'Software Engineer ', 'Software Engineer  ',
       'Junior System Administrator  ', 'Junior System Administrator',
       'Junior System Administrator ', ' Industrial Trainee ', '  ',
       'Sr. Executive', 'Senior Software Engineer  VistA',
       'Senior Software Engineer', 'Trainee Software Engineer',
       'Trainee Software Engineer ', 'Trainee Software Engineer  ',
       'Trainee Software Engineer   ', 'Trainee Software ',
       'Trainee Software Engineer Software Engineer ',
       'Trainee Software EngineerAdministrator ',
       'Trainee Software EngineerProgrammer  ',
       'Senior Software Engineer  ', 'Senior Software Engineer ',
       'Marketing Executive', 'Marketing ExecutiveAdministrator ', ' ',
       ' Software Engineer ', 'Project Lead',
       'Project Lead Software Engineer ', 'Project Lead ',
       'Project Lead   ', 'Industrial Trainee ',
       'Senior Business Analyst   ', 'Business Analyst   Healthcare',
       'Business Analyst   Healthcare  ', 'Server DBA ', 'Build Engineer',
       'Assistant Project Lead', 'Assistant Project Lead  ',
       'Programmer  ', 'Software Engineer Industrial Trainee ',
       'Software Test Engineer ', 'Software Test Engineer  ',
       'Software Test Engineer', 'Business Analyst  ',
       'Senior Project Lead',
       'Software Test Engineer   Software Engineer ',
       'Software Test Engineer Industrial Trainee ',
       'Software Test EngineerProgrammer  ',
       'Senior Software Test Engineer  ', 'Senior Software Test Engineer',
       'Software Test Engineer Software Engineer ', 'Trainee ', 'Trainee',
       'Trainee Industrial Trainee ', 'Trainee  ', 'TraineeAdministrator ',
       'Associate Tech Lead', 'Project Lead Learning Expert ',
       'Project Lead  ', 'Tech Lead', 'Tech Lead ', 'Tech Lead  ',
       'Senior Software Engineer Software Engineer ',
       'TraineeSr. Executive ', 'Trainee   ', 'Associate Technical Lead',
       'Associate Technical Lead  ', 'TraineeProgrammer  ',
       'Ability to define objectives and',
       'Associate Tech Lead   Data Visualization',
       'Associate Tech Lead   Data Visualization  ',
       'Business Development Manager  ', 'Software Engineer   ',
       'Software Engineer   Chatbot', 'Software Engineer   Chatbot ',
       'Proposal Lead  ', 'Senior Software Engineer   Dee',
       'Senior Software Engineer   Pattern Matching  ', 'Proposal Lead',
       'Senior Software Engineer   Pattern Matching',
       'Senior Technical Lead ',
       'Senior Technical Lead Software Engineer ', 'Sales Executive',
       'Market Research Analyst', 'Market Research Analyst ']
    panelMemberLi = ['2','3','4','5']
    return render_to_response('interviewParameters.html',{'applicationIdLi':applicationIdLi,'SkillsLi':SkillsLi,'PositionLi':PositionLi,'panelMemberLi':panelMemberLi})


@csrf_exempt
def getinterviewParameters(request):
    JobDescriptionNew = ''
    JobDescription = ''

    interviewId = request.POST.get('interviewId')
    checkFlag = request.POST.get('checkFlag')
    JobDescriptionNew = request.POST.get('JobDetails')
    resultdisplay = request.POST.get('JobDetails')

    if checkFlag == 'false':

        JobDescriptionNew = ''
        JobDescription = JobOpenings.objects.get(ApplicationInterviewId=interviewId).External_Job_Description
        NewJD = JobOpenings.objects.get(ApplicationInterviewId=interviewId).jd_old
        JobDescription = JobDescription.replace('year','years')
        JobDescription = JobDescription.replace('Year', 'years')
        JobDescription = JobDescription.replace('yrs', 'years')
        JobDescription = JobDescription.replace('Yrs', 'years')
        JobDescription = JobDescription.replace('<span style="font-size:12.0pt;font-family:\'Times New Roman\'', '-')
        JobDescription = JobDescription.replace('<span style="font-size:12.0pt;font-family:\'Tahoma\'', '-')
        JobDescription = JobDescription.replace('<p style="margin-left:.25in;">', ' ')
        JobDescription = JobDescription.replace('?', ' ')
        JobDescription =  JobDescription.encode('utf-8').strip()
        NewJD = NewJD.encode('utf-8').strip()
        NewJD = re.sub(r'[\xc3\xa2\xe2\x82\xac\xc2\xa2]', " ", NewJD)
        NewJD = re.sub(r'[\xc3\x82\xc2\xa7]', " ", NewJD)
        NewJD = NewJD.lower()
        JobDescription = JobDescription.lower()
        JobDescription = re.sub(r'[\xc3\x83\xc2\xbf]', " ", JobDescription)
        NewJD = unicode(NewJD, errors='ignore')
        # url = 'http://10.0.13.61:5000/exp'
        # headers = {'Content-Type':'application/json'}
        # params= json.dumps(NewJD)
        # req = urllib2.Request(url, params, headers)
        # response = urllib2.urlopen(req)
        # result = response.read()
        # result2 = ast.literal_eval(result)
        # assert type(result2) is dict
        # print "api responce",result2["position"]

        #code to find Skills from JD
        r = Rake()
        # SkillstopWords = set(stopwords.words('tech'))
        # r.extract_keywords_from_text(JobDescription)
        # df1 = r.get_ranked_phrases_with_scores()
        # df1 = pd.DataFrame(df1)
        # df1 = df1.rename(columns={0: 'Score', 1: 'DataExtract'})
        # x = ' '.join(df1.DataExtract)
        # del df1
        # words = word_tokenize(x)
        # SkillswordsFiltered = []
        # for w in words:
        #     if w in SkillstopWords:
        #         SkillswordsFiltered.append(w)
        # SkillswordsFilteredSet = set(SkillswordsFiltered)
        # SkillswordsFiltered = list(SkillswordsFilteredSet)

        # New code to extract skills from JD - Shreyas
        dfl = pd.read_csv('/home/nehan/IPRSystemWithNewChanges/TextFiles/new_naukri_skills.txt', header=None)
        dfl[0] = dfl[0].str.lower()
        dfl[0] = dfl[0].str.rstrip('jobs')
        dfl[0] = dfl[0].str.strip()

        sklist = dfl[0].values.tolist()

        blob = TextBlob(JobDescription.lower())

        onegram = []
        twogram = []
        threegram = []
        finlist = []

        ngram_var = blob.ngrams(n=1)
        # print(ngram_var)
        for i in ngram_var:
            onegram.append(i[0])

        ngram_var2 = blob.ngrams(n=2)
        # print(ngram_var2)
        i = 0
        while i < len(ngram_var2):
            twogram.append(ngram_var2[i][0] + ' ' + ngram_var2[i][1])
            i = i + 1

        ngram_var3 = blob.ngrams(n=3)
        # print(ngram_var3)
        i = 0
        while i < len(ngram_var3):
            threegram.append(ngram_var3[i][0] + ' ' + ngram_var3[i][1] + ' ' + ngram_var3[i][2])
            i = i + 1

        for i in onegram:
            if i in sklist:
                finlist.append(i)

        for i in twogram:
            if i in sklist:
                finlist.append(i)

        for i in threegram:
            if i in sklist:
                finlist.append(i)

        SkillswordsFiltered = unique_list(finlist)


        #code to fetch the relevant skills - with condition
        # notInVoc = False
        # RelevantSkills = []
        # if len(SkillswordsFiltered) < 5 and len(SkillswordsFiltered) >= 1:
        #     try:
        #         model = gensim.models.Word2Vec.load('/home/nehan/IPRSwithExperienceandPosition/Neha-Folder/Dependencies/New-W2V-RelevantSkills')
        #         for (skill, score) in model.most_similar(SkillswordsFiltered[0]):
        #             try:
        #                 RelevantSkills.append(skill.encode('utf-8'))
        #                 notInVoc = False
        #             except IndexError:
        #                 pass
        #             continue
        #     except KeyError:
        #         print "not in vocabulary"
        #         notInVoc = True

        # code to fetch the relevant skills - without condition
        notInVoc = False
        RelevantSkills = []
        try:
            model = gensim.models.Word2Vec.load('/home/nehan/IPRSystemWithNewChanges/Models/New-W2V-RelevantSkills')
            for (skill, score) in model.most_similar(SkillswordsFiltered[0]):
                try:
                    RelevantSkills.append(skill.encode('utf-8'))
                    notInVoc = False
                except IndexError:
                    pass
                continue
        except KeyError:
            print "not in vocabulary"
            notInVoc = True

        #code to find mandatory skills
        list1 = pd.read_csv('/home/nehan/IPRSystemWithNewChanges/TextFiles/naukri_skills.txt', header=None)
        prdf = pd.read_csv('/home/nehan/IPRSystemWithNewChanges/TextFiles/priority.txt',header=None)
        prdf = prdf[0].values.tolist()

        list1[0] = list1[0].str.replace('Jobs', '')
        list1[0] = list1[0].str.lower()
        list1[0] = list1[0].str.strip()
        choices = list1[0].values.tolist()

        sentences = []
        skills = []
        final = [[], []]

        article_sentence = sent_tokenize(NewJD.lower())

        for s in article_sentence:
            for w in prdf:
                if w in s:
                    final[0].append(w)
                    final[1].append(s)

        newlist = []
        newlist = " ".join(final[1])
        newlist = newlist.rstrip('.')
        # print("new list", newlist)

        prioSkill = []
        for i in newlist.split(' '):
            if i in choices:
                prioSkill.append(i)

        pro = ",".join(prioSkill)
        prioSkill = ','.join(unique_list(pro.split(',')))
        # prioSkill = prioSkill[:3]
        # print "prio skill", prioSkill

        #Code to fetch position data
        PositionstopWords = set(stopwords.words('designations'))
        r.extract_keywords_from_text(NewJD)
        df2 = r.get_ranked_phrases_with_scores()
        df2 = pd.DataFrame(df2)
        df2 = df2.rename(columns={0: 'Score', 1: 'DataExtract'})
        PositionwordsFiltered = []
        try:
            for w in df2['DataExtract']:
                if w in PositionstopWords:
                    PositionwordsFiltered.append(w)
            PositionwordsFiltered = PositionwordsFiltered[0]
        except IndexError:
            PositionwordsFiltered = ''
        # print "jd", NewJD

        # code to fetch minimum Experience data
        url = 'http://0.0.0.0:5001/exp'
        headers = {'Content-Type': 'application/json'}
        params = json.dumps(NewJD)
        req = urllib2.Request(url, params, headers)
        response = urllib2.urlopen(req)
        result = response.read()
        result2 = ast.literal_eval(result)
        assert type(result2) is dict
        print "api responce", result2["min_experience"]
        experienceFrom = result2["min_experience"]
        experienceTo = result2["max_exp"]
        # if experienceTo == 'null':
        #     experienceTo = experienceFrom

        #Code to extract role from JD
        # text2 = NewJD
        # # print (text2)
        # text2 = text2.title()
        # text2 = text2.replace('.net', 'dot net')
        # text2 = text2.decode('utf-8')
        # x = finder.findall(text2)
        # fetched_words = []
        # newPosition = ''
        # for a, b, c in x:
        #     fetched_words.append(c)
        #
        # sentences = []
        # match = []
        # final = [[], []]
        # for a in text2.split('. '):
        #     for w in fetched_words:
        #         if w in a:
        #             final[0].append(w)
        #             final[1].append(a)
        #
        # headers = ['fetched_words', 'text']
        # sentence_df = pd.DataFrame(dict(zip(headers, final)))
        # sentence_df = sentence_df.drop_duplicates(subset='fetched_words')
        # sentence_df = sentence_df.reset_index(drop=True)
        # role = ""
        #
        # if sentence_df.shape[0] == 1:
        #     role = sentence_df.fetched_words[0]
        #     # print (role)
        # else:
        #     sentence_df['text'] = sentence_df['text'].astype(str)
        #     sentence_df['text'] = sentence_df['text'].str.replace('[^\w\s]', ' ')
        #     stop = set(stopwords.words('english'))
        #     sentence_df['text'] = sentence_df['text'].str.lower()
        #     sentence_df['text'] = sentence_df['text'].str.strip()
        #     sentence_df['new_text'] = ""
        #
        #     def remove_stop(string):
        #         new_text = [j for j in string.split() if j not in stop]
        #         return new_text
        #
        #     sentence_df['new_text'] = sentence_df['text'].apply(lambda x: remove_stop(x))
        #
        #     sentence_df['new_text'] = sentence_df['new_text'].apply(lambda x: ' '.join(x))
        #     sentence_df['new_text'] = sentence_df['new_text'].str.title()
        #     sentence_df['ext_verb'] = ""
        #
        #     def verb(string):
        #         doc = nlp(string.decode('utf8'))
        #         ret_verb = ""
        #         for token in doc:
        #             if token.pos_ == 'VERB':
        #                 ret_verb = token.text
        #         return ret_verb
        #     # print (sentence_df)
        #     sentence_df['ext_verb'] = sentence_df['new_text'].apply(lambda x: verb(x))
        #     # print (sentence_df)
        #     sentence_df['flag'] = 0
        #     sim_list = ['newcomer', 'volunteer', 'appoint', 'hire', 'employ', 'engage', 'enlist', 'obtain',
        #                     'select','assign', 'book', 'commission', 'require', 'consider']
        #
        #     def search_syn(string):
        #         a = 0
        #         try:
        #             syn_list = dictionary.synonym(string)
        #             for j in syn_list:
        #                 if j in sim_list:
        #                     a = 1
        #         except TypeError:
        #             pass
        #         return a
        #
        #     sentence_df['flag'] = sentence_df['ext_verb'].apply(lambda x: search_syn(x))
        #
        #     i = 0
        #     while i < sentence_df.shape[0]:
        #         if sentence_df['flag'][i] == 1:
        #             role = sentence_df['fetched_words'][i]
        #         elif sentence_df['flag'][i] == 0:
        #             role = sentence_df['fetched_words'][0]
        #         i = i + 1

            # print ('Role :-', role)

        # New Code to extract Role - Shreyas
        text2 = NewJD
        text2 = text2.lower()
        text2 = text2.decode('utf-8')
        jd_list = []
        list1 = []
        jd_list = text2.split('. ')

        for i in jd_list:
            doc2 = NerModel(i)
            for ent in doc2.ents:
                list1.append((ent.label_, ent.text))

        newlist = []
        for (x, y) in list1:
            newlist.append(y)

        text = ' '.join(newlist)

        x = finder.findall(text)
        role = ""
        for a, b, c in x:
            role = c

        # print ('Role :-',role)

        if role == "":
            print ('Null Part')
            text2 = text2.title()
            # text2 = text2.replace('.net','dot net')
            x = finder.findall(text2)
            fetched_words = []

            for a, b, c in x:
                fetched_words.append(c)

            sentences = []
            match = []
            final = [[], []]
            for a in text2.split('.'):
                for w in fetched_words:
                    if w in a:
                        final[0].append(w)
                        final[1].append(a)

            headers = ['fetched_words', 'text']
            sentence_df = pd.DataFrame(dict(zip(headers, final)))
            sentence_df = sentence_df.drop_duplicates(subset='fetched_words')
            sentence_df = sentence_df.reset_index(drop=True)
            role = ""

            if sentence_df.shape[0] == 1:
                role = sentence_df.fetched_words[0]
            else:
                sentence_df['text'] = sentence_df['text'].astype(str)
                sentence_df['text'] = sentence_df['text'].str.replace('[^\w\s]', '')
                stop = set(stopwords.words('english'))
                sentence_df['text'] = sentence_df['text'].str.lower()
                sentence_df['text'] = sentence_df['text'].str.strip()
                sentence_df['new_text'] = ""

                def remove_stop(string):
                    new_text = [j for j in string.split() if j not in stop]
                    return new_text

                sentence_df['new_text'] = sentence_df['text'].apply(lambda x: remove_stop(x))

                sentence_df['new_text'] = sentence_df['new_text'].apply(lambda x: ' '.join(x))
                sentence_df['new_text'] = sentence_df['new_text'].str.title()
                sentence_df['ext_verb'] = ""

                def verb(string):
                    doc = nlp(string.decode('utf-8'))
                    ret_verb = ""
                    for token in doc:
                        if token.pos_ == 'VERB':
                            ret_verb = token.text
                    return ret_verb

                sentence_df['ext_verb'] = sentence_df['new_text'].apply(lambda x: verb(x))

                sentence_df['flag'] = 0
                sim_list = ['newcomer', 'volunteer', 'appoint', 'hire', 'employ', 'engage', 'enlist', 'obtain',
                            'select', 'assign', 'book', 'commission', 'require', 'consider']

                def search_syn(string):
                    a = 0
                    try:
                        syn_list = dictionary.synonym(string)
                        for j in syn_list:
                            if j in sim_list:
                                a = 1
                    except TypeError:
                        pass
                    return a

                sentence_df['flag'] = sentence_df['ext_verb'].apply(lambda x: search_syn(x))

                i = 0
                while i < sentence_df.shape[0]:
                    if sentence_df['flag'][i] == 1:
                        role = sentence_df['fetched_words'][i]
                    elif sentence_df['flag'][i] == 0:
                        role = sentence_df['fetched_words'][0]
                    i = i + 1

        print ('Role :-', role)

        # Azure API Call for fetching Designation/Position from Role and Experience

        data = {

            "Inputs": {

                "Desg_Input":
                    {
                        "ColumnNames": ["Position", "Experience", "Designation"],
                        "Values": [[role, experienceFrom, "null"]]
                    }, },
            "GlobalParameters": {
            }
        }

        body = str.encode(json.dumps(data))

        url = 'https://ussouthcentral.services.azureml.net/workspaces/74acff4939ba4f0b860e4186e71c39c2/services/8d62e1b262e64771b3142f4171bf5599/execute?api-version=2.0&details=true'
        api_key = 'tOWSm94X8dekmDjJfw/OdF8FnCc1RUZFRprbqeOrQ8usRonb4BLJn4FYE7A5LLe8+CFWnygPmvJTj31ocYwwAw=='  # Replace this with the API key for the web service
        headers = {'Content-Type': 'application/json', 'Authorization': ('Bearer ' + api_key)}

        req = urllib2.Request(url, body, headers)

        try:
            response = urllib2.urlopen(req)

                # If you are using Python 3+, replace urllib2 with urllib.request in the above code:
                # req = urllib.request.Request(url, body, headers)
                # response = urllib.request.urlopen(req)

            result = response.read()
            # print(result)

        except urllib2.HTTPError, error:
            print("The request failed with status code: " + str(error.code))

                # Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
            print(error.info())

            print(json.loads(error.read()))

        # abc = result.decode()
        abc1 = json.loads(result)
        newPosition = abc1["Results"]["Desg_Output"]["value"]["Values"][0][2]
        #print "new position", newPosition
        # print ("Role :- ", role)
        # print ("Experience :- ", experienceFrom)
        # print ("Final Position :- ", newPosition)

    else:
        mandatorySkillsLi = []
        newPosition = ''
        JobDescription = ''
        JobDescriptionNew = JobDescriptionNew.replace('year', 'years')
        JobDescriptionNew = JobDescriptionNew.replace('Year', 'years')
        JobDescriptionNew = JobDescriptionNew.replace('yrs', 'years')
        JobDescriptionNew = JobDescriptionNew.replace('Yrs', 'years')
        JobDescriptionNew = JobDescriptionNew.replace('<span style="font-size:12.0pt;font-family:\'Times New Roman\'', '-')
        JobDescriptionNew = JobDescriptionNew.replace('<span style="font-size:12.0pt;font-family:\'Tahoma\'', '-')
        JobDescriptionNew = JobDescriptionNew.replace('<p style="margin-left:.25in;">', ' ')
        JobDescriptionNew = JobDescriptionNew.replace('?', ' ')
        JobDescriptionNew = JobDescriptionNew.encode('utf-8').strip()
        JobDescriptionNew = re.sub(r'[\xc3\xa2\xe2\x82\xac\xc2\xa2]', " ", JobDescriptionNew)
        JobDescriptionNew = JobDescriptionNew.lower()
        JobDescriptionNew = unicode(JobDescriptionNew, errors='ignore')
        # print "JD is",JobDescriptionNew

        # Code to fetch Skills from JD
        r = Rake()
        # SkillstopWords = set(stopwords.words('tech'))
        # r.extract_keywords_from_text(JobDescriptionNew)
        # df3 = r.get_ranked_phrases_with_scores()
        # df3 = pd.DataFrame(df3)
        # df3 = df3.rename(columns={0: 'Score', 1: 'DataExtract'})
        # x = ' '.join(df3.DataExtract)
        # # del df3
        # words = word_tokenize(x)
        # SkillswordsFiltered = []
        # for w in df3['DataExtract']:
        #     if w in SkillstopWords:
        #         SkillswordsFiltered.append(w)
        # SkillswordsFilteredSet = set(SkillswordsFiltered)
        # SkillswordsFiltered = list(SkillswordsFilteredSet)

        # New code to extract skills from JD - Shreyas
        dfl = pd.read_csv('/home/nehan/IPRSystemWithNewChanges/TextFiles/new_naukri_skills.txt', header=None)
        dfl[0] = dfl[0].str.lower()
        dfl[0] = dfl[0].str.rstrip('jobs')
        dfl[0] = dfl[0].str.strip()

        sklist = dfl[0].values.tolist()

        blob = TextBlob(JobDescriptionNew.lower())

        onegram = []
        twogram = []
        threegram = []
        finlist = []

        ngram_var = blob.ngrams(n=1)
        # print(ngram_var)
        for i in ngram_var:
            onegram.append(i[0])

        ngram_var2 = blob.ngrams(n=2)
        # print(ngram_var2)
        i = 0
        while i < len(ngram_var2):
            twogram.append(ngram_var2[i][0] + ' ' + ngram_var2[i][1])
            i = i + 1

        ngram_var3 = blob.ngrams(n=3)
        # print(ngram_var3)
        i = 0
        while i < len(ngram_var3):
            threegram.append(ngram_var3[i][0] + ' ' + ngram_var3[i][1] + ' ' + ngram_var3[i][2])
            i = i + 1

        for i in onegram:
            if i in sklist:
                finlist.append(i)

        for i in twogram:
            if i in sklist:
                finlist.append(i)

        for i in threegram:
            if i in sklist:
                finlist.append(i)

        SkillswordsFiltered = unique_list(finlist)
        print (SkillswordsFiltered)

        # code to fetch the relevant skills - with condition
        # RelevantSkills = []
        # notInVoc = False
        # if len(SkillswordsFiltered) < 5 and len(SkillswordsFiltered) >= 1:
        #     try:
        #         model = gensim.models.Word2Vec.load('/home/nehan/IPRSwithExperienceandPosition/Neha-Folder/Dependencies/New-W2V-RelevantSkills')
        #         for (skill, score) in model.most_similar(SkillswordsFiltered[0]):
        #             try:
        #                 RelevantSkills.append(skill.encode('utf-8'))
        #                 notInVoc = False
        #             except IndexError:
        #                 pass
        #             continue
        #     except KeyError:
        #         print "not in vocabulary"
        #         notInVoc = True

        # code to fetch the relevant skills - without condition
        notInVoc = False
        RelevantSkills = []
        try:
            model = gensim.models.Word2Vec.load('/home/nehan/IPRSystemWithNewChanges/Models/New-W2V-RelevantSkills')
            for (skill, score) in model.most_similar(SkillswordsFiltered[0]):
                try:
                    RelevantSkills.append(skill.encode('utf-8'))
                    notInVoc = False
                except IndexError:
                    pass
                continue
        except KeyError:
            print "not in vocabulary"
            notInVoc = True

        # Code to fetch position data
        PositionstopWords = set(stopwords.words('designations'))
        r.extract_keywords_from_text(JobDescriptionNew)
        df4 = r.get_ranked_phrases_with_scores()
        df4 = pd.DataFrame(df4)
        df4 = df4.rename(columns={0: 'Score', 1: 'DataExtract'})
        PositionwordsFiltered = []
        for w in df4['DataExtract']:
            if w in PositionstopWords:
                PositionwordsFiltered.append(w)
        #PositionwordsFiltered = PositionwordsFiltered[0]

        # code to fetch Experience data
        # try:
        #     Experience = re.search('Experience(.+)years', JobDescriptionNew, flags=re.IGNORECASE)
        #     ExtractedExperience = Experience.group()
        #     experienceFrom = ''
        #     no = re.findall('[-+]?\d*\.\d+|\d+', ExtractedExperience)
        #     experienceFrom = no[0]
        # except AttributeError:
        #     ExtractedExperience = ''
        #     experienceFrom = ''

        #shreyas's code for exp
        # sent_list = []
        # word_list = []
        # num_list = []
        # for i in JobDescriptionNew.split('.'):
        #     sent_list.append(i.split(' '))
        # # print "sent list",sent_list
        # model = gensim.models.Word2Vec(sent_list, min_count=1, iter=1000, size=200)
        # x = model.predict_output_word(
        #     ['experience', 'minimum', 'total', 'atleast', 'yrs', 'year', 'years', 'yearss', 'of', '-', '+', ':'])
        # for word, score in x:
        #     word_list.append(word)
        # text_str = " ".join(word_list)
        # text_str = text_str.lower()
        # text_str = text_str.replace('-', ' ')
        # text_str = text_str.strip()
        # global nlp
        # doc = nlp(text_str.decode('utf8'))
        # # print "JD",doc
        # for token in doc:
        #     if token.pos_ == 'NUM':
        #         num_list.append(token.text)
        # # print "num list",num_list
        # experienceFrom = min(num_list or [0])
        # experienceTo = max(num_list or [1])
        # if experienceTo == 'null':
        #     experienceTo = experienceFrom
        # del sent_list
        # del word_list


        # code to fetch minimum Experience data
        print "job desc",JobDescriptionNew
        url = 'http://0.0.0.0:5001/exp'
        headers = {'Content-Type': 'application/json'}
        params = json.dumps(JobDescriptionNew)
        req = urllib2.Request(url, params, headers)
        response = urllib2.urlopen(req)
        result = response.read()
        result2 = ast.literal_eval(result)
        assert type(result2) is dict
        print "api responce", result2["min_experience"]
        experienceFrom = result2["min_experience"]
        experienceTo = result2["max_exp"]


        # Code to extract role from JD
        # text2 = JobDescriptionNew
        # # print (text2)
        # text2 = text2.title()
        # text2 = text2.replace('.net', 'dot net')
        # text2 = text2.decode('utf-8')
        # x = finder.findall(text2)
        # fetched_words = []
        #
        # for a, b, c in x:
        #     fetched_words.append(c)
        #
        # sentences = []
        # match = []
        # final = [[], []]
        # for a in text2.split('. '):
        #     for w in fetched_words:
        #         if w in a:
        #             final[0].append(w)
        #             final[1].append(a)
        #
        # headers = ['fetched_words', 'text']
        # sentence_df = pd.DataFrame(dict(zip(headers, final)))
        # sentence_df = sentence_df.drop_duplicates(subset='fetched_words')
        # sentence_df = sentence_df.reset_index(drop=True)
        # role = ""
        #
        # if sentence_df.shape[0] == 1:
        #     role = sentence_df.fetched_words[0]
        #     # print "role",role
        # else:
        #     sentence_df['text'] = sentence_df['text'].astype(str)
        #     sentence_df['text'] = sentence_df['text'].str.replace('[^\w\s]', ' ')
        #     stop = set(stopwords.words('english'))
        #     sentence_df['text'] = sentence_df['text'].str.lower()
        #     sentence_df['text'] = sentence_df['text'].str.strip()
        #     sentence_df['new_text'] = ""
        #
        #     def remove_stop(string):
        #         new_text = [j for j in string.split() if j not in stop]
        #         return new_text
        #
        #     sentence_df['new_text'] = sentence_df['text'].apply(lambda x: remove_stop(x))
        #
        #     sentence_df['new_text'] = sentence_df['new_text'].apply(lambda x: ' '.join(x))
        #     sentence_df['new_text'] = sentence_df['new_text'].str.title()
        #     sentence_df['ext_verb'] = ""
        #
        #     def verb(string):
        #         doc = nlp(string.decode('utf8'))
        #         ret_verb = ""
        #         for token in doc:
        #             if token.pos_ == 'VERB':
        #                 ret_verb = token.text
        #         return ret_verb
        #     # print (sentence_df)
        #     sentence_df['ext_verb'] = sentence_df['new_text'].apply(lambda x: verb(x))
        #
        #     sentence_df['flag'] = 0
        #     sim_list = ['newcomer', 'volunteer', 'appoint', 'hire', 'employ', 'engage', 'enlist', 'obtain',
        #                 'select', 'assign', 'book', 'commission', 'require', 'consider']
        #
        #     def search_syn(string):
        #         a = 0
        #         try:
        #             syn_list = dictionary.synonym(string)
        #             for j in syn_list:
        #                 if j in sim_list:
        #                     a = 1
        #         except TypeError:
        #             pass
        #         return a
        #
        #     sentence_df['flag'] = sentence_df['ext_verb'].apply(lambda x: search_syn(x))
        #
        #     i = 0
        #     while i < sentence_df.shape[0]:
        #         if sentence_df['flag'][i] == 1:
        #             role = sentence_df['fetched_words'][i]
        #         elif sentence_df['flag'][i] == 0:
        #             role = sentence_df['fetched_words'][0]
        #         i = i + 1
        #
        #     # print ('Role :-', role)

        # New Code to extract Role - Shreyas
        text2 = JobDescriptionNew
        text2 = text2.lower()
        text2 = text2.decode('utf-8')
        jd_list = []
        list1 = []
        jd_list = text2.split('. ')

        for i in jd_list:
            doc2 = NerModel(i)
            for ent in doc2.ents:
                list1.append((ent.label_, ent.text))

        newlist = []
        for (x, y) in list1:
            newlist.append(y)

        text = ' '.join(newlist)

        x = finder.findall(text)
        role = ""
        for a, b, c in x:
            role = c

        # print ('Role :-',role)

        if role == "":
            print ('Null Part')
            text2 = text2.title()
            # text2 = text2.replace('.net','dot net')
            x = finder.findall(text2)
            fetched_words = []

            for a, b, c in x:
                fetched_words.append(c)

            sentences = []
            match = []
            final = [[], []]
            for a in text2.split('.'):
                for w in fetched_words:
                    if w in a:
                        final[0].append(w)
                        final[1].append(a)

            headers = ['fetched_words', 'text']
            sentence_df = pd.DataFrame(dict(zip(headers, final)))
            sentence_df = sentence_df.drop_duplicates(subset='fetched_words')
            sentence_df = sentence_df.reset_index(drop=True)
            role = ""

            if sentence_df.shape[0] == 1:
                role = sentence_df.fetched_words[0]
            else:
                sentence_df['text'] = sentence_df['text'].astype(str)
                sentence_df['text'] = sentence_df['text'].str.replace('[^\w\s]', '')
                stop = set(stopwords.words('english'))
                sentence_df['text'] = sentence_df['text'].str.lower()
                sentence_df['text'] = sentence_df['text'].str.strip()
                sentence_df['new_text'] = ""

                def remove_stop(string):
                    new_text = [j for j in string.split() if j not in stop]
                    return new_text

                sentence_df['new_text'] = sentence_df['text'].apply(lambda x: remove_stop(x))

                sentence_df['new_text'] = sentence_df['new_text'].apply(lambda x: ' '.join(x))
                sentence_df['new_text'] = sentence_df['new_text'].str.title()
                sentence_df['ext_verb'] = ""

                def verb(string):
                    doc = nlp(string.decode('utf-8'))
                    ret_verb = ""
                    for token in doc:
                        if token.pos_ == 'VERB':
                            ret_verb = token.text
                    return ret_verb

                sentence_df['ext_verb'] = sentence_df['new_text'].apply(lambda x: verb(x))

                sentence_df['flag'] = 0
                sim_list = ['newcomer', 'volunteer', 'appoint', 'hire', 'employ', 'engage', 'enlist', 'obtain',
                            'select', 'assign', 'book', 'commission', 'require', 'consider']

                def search_syn(string):
                    a = 0
                    try:
                        syn_list = dictionary.synonym(string)
                        for j in syn_list:
                            if j in sim_list:
                                a = 1
                    except TypeError:
                        pass
                    return a

                sentence_df['flag'] = sentence_df['ext_verb'].apply(lambda x: search_syn(x))

                i = 0
                while i < sentence_df.shape[0]:
                    if sentence_df['flag'][i] == 1:
                        role = sentence_df['fetched_words'][i]
                    elif sentence_df['flag'][i] == 0:
                        role = sentence_df['fetched_words'][0]
                    i = i + 1

        print ('Role :-', role)

        # Azure API Call to fetch position from role and experience
        experienceFrom = str(experienceFrom)
        data = {

            "Inputs": {

                "Desg_Input":
                    {
                        "ColumnNames": ["Position", "Experience", "Designation"],
                        "Values": [[role, experienceFrom, "null"]]
                    }, },
            "GlobalParameters": {
            }
        }

        body = str.encode(json.dumps(data))

        url = 'https://ussouthcentral.services.azureml.net/workspaces/74acff4939ba4f0b860e4186e71c39c2/services/8d62e1b262e64771b3142f4171bf5599/execute?api-version=2.0&details=true'
        api_key = 'tOWSm94X8dekmDjJfw/OdF8FnCc1RUZFRprbqeOrQ8usRonb4BLJn4FYE7A5LLe8+CFWnygPmvJTj31ocYwwAw=='  # Replace this with the API key for the web service
        headers = {'Content-Type': 'application/json', 'Authorization': ('Bearer ' + api_key)}

        req = urllib2.Request(url, body, headers)

        try:
            response = urllib2.urlopen(req)

                # If you are using Python 3+, replace urllib2 with urllib.request in the above code:
                # req = urllib.request.Request(url, body, headers)
                # response = urllib.request.urlopen(req)

            result = response.read()
            # print(result)
        except urllib2.HTTPError, error:
            print("The request failed with status code: " + str(error.code))
                # Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
            print(error.info())

            print(json.loads(error.read()))

        abc1 = json.loads(result)
        newPosition = abc1["Results"]["Desg_Output"]["value"]["Values"][0][2]

        # code to find mandatory skills
        list1 = pd.read_csv('/home/nehan/IPRSystemWithNewChanges/TextFiles/naukri_skills.txt',header=None)
        prdf = pd.read_csv('/home/nehan/IPRSystemWithNewChanges/TextFiles/priority.txt',header=None)
        prdf = prdf[0].values.tolist()
        list1[0] = list1[0].str.replace('Jobs', '')
        list1[0] = list1[0].str.lower()
        list1[0] = list1[0].str.strip()
        choices = list1[0].values.tolist()
        sentences = []
        skills = []
        final = [[], []]

        article_sentence = sent_tokenize(JobDescriptionNew.lower())

        for s in article_sentence:
            for w in prdf:
                if w in s:
                    final[0].append(w)
                    final[1].append(s)

        newlist = []
        newlist = " ".join(final[1])
        newlist = newlist.rstrip('.')


        prioSkill = []
        for i in newlist.split(' '):
            if i in choices:
                prioSkill.append(i)

        pro = ",".join(prioSkill)
        prioSkill = ','.join(unique_list(pro.split(',')))
        # prioSkill = prioSkill[:3]
        # print "prio skill", prioSkill
    return HttpResponse(json.dumps({"JobDescription":JobDescription,"ExtractedExperience":experienceFrom,"Skills":SkillswordsFiltered,"RelevantSkills":RelevantSkills,
                                    "Position":PositionwordsFiltered,"prioSkill":prioSkill,"notInVoc":notInVoc,"role":role,"newPosition":newPosition,"experienceTo":experienceTo}), content_type="application/json")



@csrf_exempt
def getpanelRecommendation(request):
    response = ""
    PanelMembers = []
    exp = request.POST.get('experience').encode('utf-8')
    exp = int(exp)
    skills = request.POST.get('newSkillSet').encode('utf-8')
    role = request.POST.get('position').encode('utf-8')
    role = role.title()
    nos = request.POST.get('noOfPanels').encode('utf-8')
    nos = int(nos)
    data = {
            "exp":exp,
            "skills":skills,
            "role":role,
            "nos":nos,
        }


    params= str.encode(json.dumps(data))
    url = 'http://0.0.0.0:5000/exp'
    headers = {'Content-Type': 'application/json'}
    req = urllib2.Request(url, params, headers)
    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError:
        pass

    PanelMembers = json.loads(response.read())
    return HttpResponse(json.dumps({"PanelMembers": PanelMembers}), content_type="application/json")



