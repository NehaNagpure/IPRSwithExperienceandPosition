from django.contrib import admin

# Register your models here.
from .models import JobOpenings


class JobOpeningsAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ("ApplicationInterviewId","External_Job_Description","jd_old")
    search_fields = list_display
    list_display_links = list_display

admin.site.register(JobOpenings,JobOpeningsAdmin)
