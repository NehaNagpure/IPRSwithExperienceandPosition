from __future__ import unicode_literals

from django.apps import AppConfig


class InterviewpanelAppConfig(AppConfig):
    name = 'interviewpanel_app'
