# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-10-06 11:32
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('interviewpanel_app', '0006_jobopenings_new_external_job_description'),
    ]

    operations = [
        migrations.RenameField(
            model_name='jobopenings',
            old_name='New_External_Job_Description',
            new_name='jd_old',
        ),
    ]
