from rest_framework import serializers
from .models import JobOpenings
# from rest_framework import JobOpenings


class JobOpeningsSerializers(serializers.ModelSerializer):

    class Meta:
        model = JobOpenings
        # fields = ('ApplicationInterviewId','External_Job_Description','jd_old')
        fields = '__all__'

