"""InterviewPanelRecommender URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from interviewpanel_app.views import *
from rest_framework.urlpatterns import format_suffix_patterns
from interviewpanel_app import views



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^jobdescriptions/', views.jobDescriptionList.as_view()),

    # url(r'^interview/list/', interviewList, name="interview_list"),
    url(r'^$', 'interviewpanel_app.views.interviewList'),
    # url(r'^(?i)interview/details/', getinterviewDetails, name="interview_details"),
    url(r'^(?i)interview/parameters/', getinterviewParameters, name="interview_parameters"),
    url(r'^(?i)recommend/panel/', getpanelRecommendation, name="panel_recommendtion"),
    # url(r'^(?i)typical/profile/', getTypicalInterviewerProfile, name="typical_profile"),
]