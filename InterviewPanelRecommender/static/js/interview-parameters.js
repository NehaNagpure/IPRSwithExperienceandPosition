$(document).ready(function() {
$("#ManualJobDescription").hide();
$("#JobDescription").hide();
$("#JobReqId").hide();
$("#infoMsg").hide();
$('#jobdetailloaderBtn').hide();
$('#typicalprofileloaderBtn').hide();
$('#recommendpanelloaderBtn').hide();
$('#MandatorySkills').hide();

$('#tokenfield').tokenfield({
              autocomplete: {
                source: ['C','C++','Java','Python'],
                delay: 100
              },
              showAutocompleteOnFocus: true
            });
$('#AssociatedSkills').tokenfield();
//$('#MandatorySkills').tokenfield();

var checkFlag = false;
 $('input:radio').change(function(){
    var radioButtonValue = $('input[name=optradio]:checked').val();
     if(radioButtonValue == 'radioForappId'){
         checkFlag = false;
         $("#experience").val('');
         $("#experienceTo").val('');
         $("#skills").val('');
//         $("#position").val('');
         $("#role").val('');
         $("#newPosition").val('');
         $("#tokenfield").tokenfield('setTokens', []);
         $("#AssociatedSkills").tokenfield('setTokens', []);
         $("#MandatorySkills").tokenfield('setTokens', []);
         $("#ManualJobDescription").html('');
         $("#ManualJobDescription").hide();
         $("#JobReqId").show();
         $("#JobDescription").show();
     }
      else if(radioButtonValue== 'radioForManual'){
            checkFlag = true;
           $("#experience").val('');
           $("#experienceTo").val('');
           $("#skills").val('');
//           $("#position").val('');
           $("#role").val('');
           $("#newPosition").val('');
           $("#JobDescription").html('');
           $("#tokenfield").tokenfield('setTokens', []);
           $("#AssociatedSkills").tokenfield('setTokens', []);
           $("#MandatorySkills").tokenfield('setTokens', []);
           $("#JobDescription").hide();
           $("#ManualJobDescription").show();
           $("#JobReqId").hide();
    }
    });

$('#jobdetailBtn').click(function(e) {
    interviewId=$('#JobReqId').val();
    JobDetails=$('#ManualJobDescription').text();

    $('#jobdetailloaderBtn').show();
    if (checkFlag == false){
     $.ajax({
      type: "POST",
        url: "/interview/parameters/",
        data: {
            "interviewId":interviewId,
             "checkFlag":checkFlag
            },
         success: function(data){
             if(data["notInVoc"] == true){
                $("#infoMsg").show();
            }
           $('#ReccomendationTable tr').has('td').remove();
           $("#myTable").find("td:gt(0)").remove();
            $('#jobdetailloaderBtn').hide();
            $("#JobDescription").html('');
            $("#tokenfield").tokenfield('setTokens', []);
            $("#AssociatedSkills").tokenfield('setTokens', []);
            $("#MandatorySkills").tokenfield('setTokens', []);
            $("#experience").val('');
            $("#skills").val('');
//            $("#position").val('');
            $("#JobDescription").append("JobDescription: "+data["JobDescription"]+"<br/>");
            $("#experience").val(data["ExtractedExperience"]);
            $("#experienceTo").val(data["experienceTo"]);
//            $("#position").val(data["Position"]);
            $("#role").val(data["role"]);
            $("#newPosition").val(data["newPosition"]);
            $('#tokenfield').tokenfield('setTokens', data["Skills"]);
            $("#AssociatedSkills").tokenfield('setTokens', data["RelevantSkills"]);
            $("#MandatorySkills").tokenfield('setTokens', data["prioSkill"]);
                     }
                 });
     }
     else{
         $.ajax({
      type: "POST",
        url: "/interview/parameters/",
        data: {
            "JobDetails":JobDetails,
            "checkFlag":checkFlag
            },
         success: function(data){
              if(data["notInVoc"] == true){
                $("#infoMsg").show();
            }
            $('#ReccomendationTable tr').has('td').remove();
            $('#jobdetailloaderBtn').hide();
            $("#experience").val('');
            $("#skills").val('');
//            $("#position").val('');
            $("#tokenfield").tokenfield('setTokens', []);
            $("#AssociatedSkills").tokenfield('setTokens', []);
            $("#MandatorySkills").tokenfield('setTokens', []);
            $("#experience").val(data["ExtractedExperience"]);
            $("#experienceTo").val(data["experienceTo"]);
//            $("#position").val(data["Position"]);
            $("#role").val(data["role"]);
            $("#newPosition").val(data["newPosition"]);
            $('#tokenfield').tokenfield('setTokens', data["Skills"]);
            $("#AssociatedSkills").tokenfield('setTokens', data["RelevantSkills"]);
            $("#MandatorySkills").tokenfield('setTokens', data["prioSkill"]);
         }

     });
     }
});

//$('#ProfileBtn').click(function(e) {
//    $('#typicalprofileloaderBtn').show();
//    position=$('#newPosition').val();
//    experience=$('#experience').val();
//    noOfPanels=$('#count-of-panel-members').val();
//    averageRating=$('#average-rating').val();
//    int_count=$('#interview-count').val();
//    tokenSkills  = $('#tokenfield').tokenfield('getTokensList');
//    relevantSkills = $('#AssociatedSkills').tokenfield('getTokensList');
//    newSkillSet = tokenSkills+","+relevantSkills;
//     $.ajax({
//      type: "POST",
//        url: "/typical/profile/",
//        data: {
//            "position":position,
//            "experience":experience,
//            "noOfPanels":noOfPanels,
////            "averageRating":averageRating,
//            "int_count":int_count,
//            "tokenSkills":tokenSkills,
//            "newSkillSet":newSkillSet
//            },
//         success: function(data){
//             $('#typicalprofileloaderBtn').hide();
//            $("#myTable tr").remove();
//             for (var i=0; i<data["result"].length; i++)
//            {
//
//                     $("#myTable").append('<tr><th>Title</th><td>Value</td></tr>'+
//                     '<tr><th>Designation</th><td>'+data["result"][i][0]+
//                 '</td></tr><tr><th>Primary Skill</th><td>'+data["result"][i][1]+
//                 '</td></tr><tr><th>Other Skills</th><td>'+data["result"][i][2]+
//                 '</td></tr><tr><th>Total Experience (years)</th><td>'+data["result"][i][3]+
//                 '</td></tr><tr><th>Peers</th><td>'+data["result"][i][4]+
//                 '</td></tr><tr><th>Count of Matrix Managers</th><td>'+data["result"][i][5]+
//                 '</td></tr><tr><th>Count of Directs</th><td>'+data["result"][i][6]+'</td></tr>');
//
//            }
//
//         }
//
//     });
//});

$('#ReccomendBtn').click(function(e) {
    position=$('#newPosition').val();
    experience=$('#experience').val();
    noOfPanels=$('#count-of-panel-members').val();
    averageRating=$('#average-rating').val();
    int_count=$('#interview-count').val();
    tokenSkills  = $('#tokenfield').tokenfield('getTokensList');
    relevantSkills = $('#AssociatedSkills').tokenfield('getTokensList');
    newSkillSet = tokenSkills+","+relevantSkills;
    $('#recommendpanelloaderBtn').show();
    nos_panel=$('#count-of-panel-members').val();
     $.ajax({
      type: "POST",
        url: "/recommend/panel/",
        data: {
           "position":position,
            "experience":experience,
            "noOfPanels":noOfPanels,
            "newSkillSet":newSkillSet
            },
         success: function(data){
            $('#recommendpanelloaderBtn').hide();
            $("#ReccomendationTable tr").remove();
            $("table.ReccomendationTable").width(1110);

            $("#ReccomendationTable").append('<tr><th style="width:10px;">Name</th><th style="width:10px;">Designation</th><th style="width:10px;">Total Experience (years)</th><th style="width:5px;">Skills</th></tr>');
             for (var i=0; i<data["PanelMembers"].length; i++)
            {
                 $("#ReccomendationTable").append('<tr><td style="width:10px;">'+data["PanelMembers"][i][0]+
                 '</td><td style="width:10px;">'+data["PanelMembers"][i][1]+'</td><td style="width:10px;">'+
                 data["PanelMembers"][i][2]+'</td><td style="width:68px; word-wrap:break-word;">'+data["PanelMembers"][i][3]+'</td></tr>');
            }


         }

     });
});

});